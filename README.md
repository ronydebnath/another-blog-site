### Introduction
- Created a simple django application which will return a JSON response while accessing the application with localhost:8000 or http://server_ip_address:8000

### Using the Application

Run locally:

```sh
$ docker-compose up -d --build
```

Verify [http://localhost:8000/](http://localhost:8000/) works as expected:

```json
{
  "Blog_Title": "Hello World!"
}
```
